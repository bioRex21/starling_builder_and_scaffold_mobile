package starlingbuilder.myLayouts 
{
	/**
	 * ...
	 * @author Abraham Vazquez
	 */
	public class EmbeddedAssets 
	{
		[Embed(source = "ui_test_1/textures/PNG/blue_sliderRight.png")]
		public static const blue_sliderRight:Class;
		
		[Embed(source="ui_test_1/textures/PNG/green_button07.png")]
		public static const green_button07:Class;
		
		[Embed(source="ui_test_1/textures/PNG/green_button08.png")]
		public static const green_button08:Class;
		
		public function EmbeddedAssets() 
		{
			
		}
		
	}

}