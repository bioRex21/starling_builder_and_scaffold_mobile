package starlingbuilder.myLayouts 
{
	/**
	 * ...
	 * @author Abraham Vazquez
	 */
	public class EmbeddedLayouts 
	{
		[Embed(source = "ui_test_1_save/ui_test_1_src", mimeType = "application/octet-stream")]
		public static const test1:Class;
		
		public function EmbeddedLayouts() 
		{
			
		}
		
	}

}